﻿using UnityEngine;

namespace PRONE
{
    public class Enemy : Entity, IDamagable
    {
        [SerializeField]
        private int currentHealth;
        [SerializeField]
        private int attack;
        [SerializeField]
        private bool isAlive;

        public int CurrentHealth
        {
            get { return currentHealth;}
            set { currentHealth = value; }
        }
        
        public int Attack
        {
            get { return attack;}
            set { attack = value; }
        }
        
        public bool IsAlive
        {
            get { return isAlive;}
            set { isAlive = value; }
        }
        
        public void Damage(int damage)
        {
            if (CurrentHealth > 0)
            {
                CurrentHealth -= damage;
            } 
            else if (CurrentHealth <= 0)
            {
                CurrentHealth = 0;
                IsAlive = false;
                Log("Name: " + EntityName + "Lives: " + CurrentHealth);
                return;
            }
            
            if (CurrentHealth <= 0)
            {
                CurrentHealth = 0;
                IsAlive = false;
                Log("Name: " + EntityName + "Lives: " + CurrentHealth);
            }
            
            Log("Enemy '" + EntityName + "' was damaged and has now " + CurrentHealth + " lives");
        }
    }
}