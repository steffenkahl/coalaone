﻿namespace PRONE
{
    public interface IAttack
    {
        void AttackEnemy(Enemy enemy, int damage);
    }
}