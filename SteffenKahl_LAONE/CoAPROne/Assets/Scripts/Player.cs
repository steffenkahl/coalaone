﻿using System;
using UnityEngine;

namespace PRONE
{
    public class Player : Entity, IAttack
    {
        [SerializeField]
        private int currentHealth;
        [SerializeField]
        private int attack;
        [SerializeField]
        private bool isAlive;
        [SerializeField]
        private Enemy enemyEntity;

        public int CurrentHealth
        {
            get { return currentHealth;}
            set { currentHealth = value; }
        }
        
        public int Attack
        {
            get { return attack;}
            set { attack = value; }
        }
        
        public bool IsAlive
        {
            get { return isAlive;}
            set { isAlive = value; }
        }
        
        public Enemy EnemyEntity
        {
            get { return enemyEntity;}
            set { enemyEntity = value; }
        }

        private void Start()
        {
            Log("Playername: " + EntityName);
            Log("Health: " + CurrentHealth);
            Log("Attackdamage: " + Attack);
            AttackEnemy(EnemyEntity, Attack);
        }

        public void AttackEnemy(Enemy enemy, int damage)
        {
            if (IsAlive)
            {
                enemy.Damage(Attack);
            }
        }
    }
}