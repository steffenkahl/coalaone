﻿using System;
using UnityEngine;

namespace PRONE
{
    public class EntityManager : MonoBehaviour, ILog
    {
        [SerializeField]
        private Entity[] entities;

        public Entity[] Entities
        {
            get { return entities; }
            set { entities = value; }
        }

        private void Start()
        {
            Log("Registering the names of entities...");
            for (int i = 0; i < Entities.Length; i++)
            {
                Log("Current Position in Loop: " + i + ". Current Entity: " + Entities[i].EntityName);
            }
        }

        public void Log(object loggedValue)
        {
            Debug.Log(loggedValue);
        }
    }
}